@extends('Sistema.template.header')
@section('content')
 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

</head>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">

				<!--Our form will go inside here...>

				<!--end col--></div>
				<!--end row--></div>
				<!--end container--></div>
				<!--Latest jQuery Core Library-->
				<script src="http://code.jquery.com/jquery-latest.min.js">
				</script>
				<!--Bootstrap-->
				<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

				<div class="col-md-8 center-block">

				<p class="required small">(*) Campos Obrigatórios</p>
				<br><br>
					@if(isset($errors) && count($errors)>0)
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
							<p>{{$error}}</p>
						@endforeach
					</div>
					@endif

				<!--begin HTML Form-->
					<form class="form-horizontal" role="form" action="{{route('clientes.store')}}" method="post">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<div class="form-group">
							<label for="nome" class="col-sm-3 control-label"><span class="required">*</span> Nome Completo:</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="nome" name="nome" value="{{ old('nome') }}" required="required" autocomplete="off" maxlength="90" >
							</div>
						</div>

						<div class="form-group">
							<label for="cnpj" class="col-sm-3 control-label"><span class="required">*</span> CNPJ: </label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="cnpj" name="cnpj" value="{{ old('nome') }}" required="required" autocomplete="off" maxlength="20">
								<small>Somente números</small>
							</div>

						</div>

						<div class="form-group">
							<label for="inscricao" class="col-sm-3 control-label"><span class="required">*</span>Inscrição Estadual: </label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="inscricao" name="inscricao_estadual" value="{{ old('inscricao_estadual') }}" autocomplete="off" maxlength="12" required="required">
								<small>Somente números</small>
							</div>
						</div>

						<div class="form-group">
							<label for="telefone" class="col-sm-3 control-label"><span class="required">*</span>Telefone: </label>
							<div class="col-sm-9">
								<input type="tel" class="form-control" id="telefone" name="telefone" value="{{ old('telefone') }}" autocomplete="off" maxlength="15" required="required">
								<small>Somente números</small>
							</div>
						</div>

						<div class="form-group">
							<label for="celular" class="col-sm-3 control-label"><span class="required">*</span>Celular: </label>
							<div class="col-sm-9">
								<input type="tel" class="form-control" id="celular" name="celular" value="{{ old('celular') }}" autocomplete="off" maxlength="15" required="required">
								<small>Somente números</small>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="col-sm-3 control-label"><span class="required">*</span>Email: </label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" autocomplete="off" maxlength="90" required="required">
							</div>
						</div>
						<h4>Endereço</h4>

						<div class="form-group">
							<label for="telefone" class="col-sm-3 control-label"><span class="required">*</span>Logradouro: </label>
							<div class="col-sm-9">
								<input type="tel" class="form-control" id="logradouro" name="logradouro" value="{{ old('logradouro') }}" autocomplete="off" maxlength="150" required="required">
							</div>
						</div>

						<div class="form-group">
							<label for="celular" class="col-sm-3 control-label"><span class="required">*</span>Número: </label>
							<div class="col-sm-2">
								<input type="number" min="1" class="form-control" id="numero" name="numero" value="{{ old('numero') }}" required="required">
							</div>
						</div>

						<div class="form-group">
							<label for="bairro" class="col-sm-3 control-label"><span class="required">*</span>Bairro: </label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="bairro" name="bairro" value="{{ old('bairro') }}" autocomplete="off" required="required" maxlength="45">
							</div>
						</div>

						<div class="form-group">
							<label for="telefone" class="col-sm-3 control-label"><span class="required">*</span>Complemento: </label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="complemento" name="complemento" value="{{ old('complemento') }}" autocomplete="off"  required="required">
							</div>
						</div>

						<div class="form-group">
							<label for="celular" class="col-sm-3 control-label"><span class="required">*</span>Cidade: </label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="cidade" name="cidade" value="{{ old('cidade') }}" autocomplete="off" required="required">
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="col-sm-3 control-label"><span class="required">*</span>UF: </label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="uf" name="uf" value="{{ old('uf') }}" autocomplete="off" required="required" maxlength="2">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6 col-sm-offset-3">
								<button type="submit" id="submit" name="submit" class="btn-lg btn-primary btn-block">Salvar</button>
							</div>
						</div>
					</form>
				</div>
				
@endsection