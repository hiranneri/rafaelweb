@extends('Sistema.template.header-pesquisar')
@section('content')
  <div class="box">
  <div class="box-header">
    <h3 class="box-title">Clientes</h3>
  </div>
  <div class="box-body table-responsive">
    <table id="table_clientes" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Nome</th>
          <th>CNPJ</th>
          <th>Inscrição Estadual</th>
          <th>Telefone / Celular</th>
          <th>Email</th>
          <th>Logradouro,Número</th>
          <th>Bairro-Cidade</th>
          <th>UF</th>
        </tr>
      </thead>
      <tbody>
        @foreach($clientes as $cliente)
        <tr>
          <td>{{$cliente->nome}}</td>
          <td>{{$cliente->cnpj}}</td>
          <td>{{$cliente->inscricao_estadual}}</td>
          <td>{{$cliente->telefone}} / {{$cliente->celular}}</td>
          <td>{{$cliente->email}}</td>
          <td>{{$cliente->endereco->logradouro}},{{$cliente->endereco->numero}}</td>
          <td>{{$cliente->endereco->bairro}} - {{$cliente->endereco->cidade}}</td>
          <td>
          @foreach($enderecosUF as $enderecoUF)
            @if($enderecoUF->uf->enderecos_id ==$cliente->endereco->id)
              {{ $enderecoUF->uf->uf }}
            @endif
          @endforeach
          </td>

        </tr>
        @endforeach

      </tbody>

  </table>

  </div>
  </div>
  <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>

  <script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#table_clientes').DataTable();
  } );
  </script>
@endsection