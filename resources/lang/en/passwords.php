<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha deve ser no mínimo 6 caracteres.',
    'reset' => 'Sua senha foi alterada com sucesso!Your password has been reset!',
    'sent' => 'Enviamos um email com a sua senha para o seu email!',
    'token' => 'This password reset token is invalid.',
    'user' => "Não conseguimos encontrar um usuário com esse endereço de email.",

];
