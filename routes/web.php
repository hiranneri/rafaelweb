<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get
Route::group(['namespace'=>'Cliente','middleware'=>'auth'], function(){
	Route::resource('/clientes','ClienteController');
	
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/auth/logout','Auth\LoginController@logout')->name('logout');


