# Hiran Neri - Área Administrativa
Projeto que faz o cadastro e pesquisa dos dados dos clientes. Foi desenvolvido utilizando no seu back-end: PHP, junto com o framework Laravel, utilizando o banco de dados MySQL.

# O que faz?
 - Cadastro de Clientes
 - Pesquisa de Clientes com Paginação 

# Observação

- Foi criado uma seed para o acesso ao sistema conforme os dados abaixo:

Usuário: Rafael
Email: rafael@rafaelweb.com.br
Senha: Rafael321
