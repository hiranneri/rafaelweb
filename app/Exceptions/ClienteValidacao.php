<?php
namespace App\Exceptions;
use Illuminate\Validation\Factory;
use Validator;
class ClienteValidacao extends Factory {
	 
	 public static function validar(array $data, array $rules, array $messages = [])
	 	{
	 		# code...
	 		$validator = Validator::make($data,$rules,$messages);
	 		return $validator;	 		
	 	}	
}
?>