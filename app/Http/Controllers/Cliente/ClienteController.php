<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cliente;
use Illuminate\Support\Facades\DB;
use App\Models\Endereco;
use App\Models\Uf;
use Illuminate\Support\Facades\Session;
use Illuminate\Exception;
use App\Exceptions\ClienteValidacao;

class ClienteController extends Controller
{
    private $clientes;
    private $mensagens;
    private $title;

    public function __construct (Cliente $cliente)
    {
        $this->clientes = $cliente;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::with('endereco')->get();
        $enderecosUF = Endereco::with('uf')->get();
        $this->title ='Pesquisa de Cliente';
        return view('Sistema.pesquisar-cliente')->with(compact('clientes','enderecosUF','title'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
      
      $this->title ='Cadastro de Cliente'; 
      return view('Sistema.cadastrar-cliente')->with(compact('title'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
       $dataForm = $request->all();
       $validacao= $this->validarCliente($dataForm);

       if(!$validacao)      
            return redirect()->back()->withInput()->withErrors($this->mensagens);
       try
       {
            \DB::transaction(function() use($request){

            $camposCliente = $request->only('nome','cnpj','inscricao_estadual','telefone','celular','email');
           
            $camposEndereco = $request->only('logradouro','numero','bairro','cidade','complemento');
           
            $uf = $request->only('uf');

            $cliente = Cliente::create($camposCliente);
            $camposEndereco['clientes_id']=$cliente->id;
            $camposEndereco=Endereco::create($camposEndereco);

            $uf['enderecos_id']=$camposEndereco->id;
            UF::create($uf);           
            });
            return redirect()->route('clientes.index');
            
        }catch(\Exception $e){
            return $e->getMessage();               
        }
        
    }
    private function validarCliente($dataForm)
    {
        $validator;
        $rules[]=Cliente::rules();
        $messages[]=Cliente::messages();

        $rules[]=Endereco::rules();
        $messages[]=Endereco::messages();

        $rules[]=Uf::rules();
        $messages[]=Uf::messages();
        
        for($i=0;$i<3;$i++){
            $validator = ClienteValidacao::validar($dataForm,$rules[$i],$messages[$i]);  
            if($validator->fails()){
                $this->mensagens = $validator;
                return false;
            }
            return true;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cliente = Cliente::find($id);
        return $cliente;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
