<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $table='tb_clientes';
    protected $fillable = ['nome', 'cnpj','inscricao_estadual','telefone','celular','email'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    
    public static function rules()
    {
        
        return $rules =[
        "nome"  =>'required|min:3|max:90|unique:tb_clientes',
        "cnpj"  => 'required|unique:tb_clientes',
        "inscricao_estadual"=>'required|numeric|unique:tb_clientes',
        "telefone"=>'numeric',
        "celular"=>'numeric|unique:tb_clientes',
    
        ];
    }
    
    public static function messages(){
        return $messages=[
    	"nome.required"				=>'O campo Nome Completo é de preenchimento obrigatório',
    	"nome.min"					=>'O campo Nome deve ter no mínimo 3 caracteres',
    	"nome.max"					=>'O campo Nome deve ter no máximo 90 caracteres',
    	"nome.unique"				=> 'Este nome já esta registrado',

        "cnpj.required"      		=> 'O campo CNPJ é de preenchimento obrigatório',
		"cnpj.unique"				=>'Este CNPJ já está registrado',

    	"inscricao_estadual.required"	=>'O campo Inscrição Estadual é obrigatório',
    	"inscricao_estadual.numeric"	=>'O campo Inscrição Estadual deve conter apenas números',
    	"inscricao_estadual.unique"		=> 'O número da Inscrição Estadual informado já está cadastrado',

        "telefone.numeric"      		=> 'O campo Telefone deve conter apenas números',
		
    	"celular.numeric"				=>'O campo Celular deve conter apenas números',    	
        "celular.unique"      			=> 'O número de celular já está cadastrado',
    	
    ];
    }

    public function endereco()
    {
    	# code...
    	return $this->hasOne('App\Models\Endereco','clientes_id');
    }
}
