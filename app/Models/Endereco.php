<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    //
    protected $table='tb_enderecos';
	protected $fillable = ['logradouro', 'numero','bairro','cidade','complemento','clientes_id'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    
    public static function rules()
    {
        return $rules =[
    	   "logradouro"		=>'required|min:3|max:45',
    	   "numero"	=> 'required|numeric',
        ];
    }

    public static function messages ()
    {
       return $messages=[
        	"logradouro.required"			=>'O campo logradouro é de preenchimento obrigatório',
        	"logradouro.min"				=>'O campo logradouro deve ter no mínimo 3 caracteres',
        	"logradouro.max"				=>'O campo logradouro deve ter no máximo 45 caracteres',
        	"numero.required"				=>'O campo Número é de preenchimento obrigatório',
            "numero.numeric"      			=> 'O campo Número deve conter apenas números',
    	
        ];
    }
    public function cliente()
    {
    	return $this->belongsTo('App\Models\Cliente');
    }
    public function uf()
    {
        # code...
        return $this->hasOne('App\Models\Uf','enderecos_id');
    }
}
