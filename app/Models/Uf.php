<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Uf extends Model
{
    //
    protected $table='tb_ufs';
    protected $fillable = ['uf','enderecos_id'];
    protected $guarded = ['id', 'created_at', 'update_at'];

    public static function rules()
    {
        
         return $rules =[
        "uf"        =>'required|max:2',     
        ];
    }
    
    public static function messages()
    {
        
        return $messages=[
            "uf.required"               =>'O campo UF é de preenchimento obrigatório',
            "uf.max"                    =>'O campo UF deve ter no máximo 2 caracteres',
        ];
    }
    

    public static function endereco()
    {
    	return $this->belongsTo('App\Models\Endereco');
    }
}
