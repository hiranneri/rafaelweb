<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
        $table->increments('id');
        $table->string('logradouro',150);
        $table->integer('numero'); 
        $table->string('bairro',45);
        $table->string('cidade',20);
        $table->text('complemento',100);
        $table->integer('clientes_id')->unsigned();
        $table->foreign('clientes_id')->references('id')->on('clientes')
        ->onUpdate('cascade')->onDelete('cascade');
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
