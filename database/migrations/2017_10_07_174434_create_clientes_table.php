<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome',90)->unique();
            $table->string('cnpj',45)->unique();
            $table->string('inscricao_estadual',45)->unique();
            $table->string('telefone',15);
            $table->string('celular',15)->unique();
            $table->string('email',100)->unique();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
